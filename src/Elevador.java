
public class Elevador {
	private int andarAtual = 0;
	private int totalAndares;
	private int capacidadeMaxima;
	private int quantidadePessoas = 0;
	
	public Elevador() {
		
	}
	
	public int getAndarAtual() {
		return andarAtual;
	}
	public void setAndarAtual(int andarAtual) {
		this.andarAtual = andarAtual;
	}
	public int getTotalAndares() {
		return totalAndares;
	}
	public void setTotalAndares(int totalAndares) {
		this.totalAndares = totalAndares;
	}
	public int getCapacidadeMaxima() {
		return capacidadeMaxima;
	}
	public void setCapacidadeMaxima(int capacidadeMaxima) {
		this.capacidadeMaxima = capacidadeMaxima;
	}
	public int getQuantidadePessoas() {
		return quantidadePessoas;
	}
	public void setQuantidadePessoas(int quantidadePessoas) {
		this.quantidadePessoas = quantidadePessoas;
	}
	
	public void inicializa(int capacidadeMaxima, int totalAndares) {
		this.capacidadeMaxima = capacidadeMaxima;
		this.totalAndares = totalAndares;
	}
	
	public void entra() {
		if (quantidadePessoas < capacidadeMaxima) {
			quantidadePessoas++;
			System.out.println("Quantidade de pessoas: " + quantidadePessoas);
		} else {
			System.out.println("Capacidade m�xima atingida");
		}
	}
	
	public void sai() {
		if (quantidadePessoas > 0) {
			quantidadePessoas--;
			System.out.println("Quantidade de pessoas: " + quantidadePessoas);
		} else {
			System.out.println("N�o existem pessoas no elevador");
		}
	}
	
	public void sobe() {
		if (andarAtual < totalAndares) {
			andarAtual++;
			System.out.println("Andar Atual: " + andarAtual);
		} else {
			System.out.println("J� est� no �ltimo andar");
		}
	}
	
	public void desce() {
		if (andarAtual != 0) {
			andarAtual--;
			System.out.println("Andar Atual: " + andarAtual);
		} else {
			System.out.println("J� est� no t�rreo");
		}
	}
}
