
public class Tela {

	public static void main(String[] args) {
		
		Elevador elevador = new Elevador();
		
		elevador.inicializa(5, 10);
		
		elevador.entra();
		elevador.entra();
		elevador.entra();
		elevador.sobe();
		elevador.sobe();
		elevador.desce();
		elevador.desce();
	}

}
